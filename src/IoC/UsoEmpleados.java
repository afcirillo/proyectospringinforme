package IoC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleados {

	public static void main(String[] args) {
		
		/*
		//Creacion de objetos de tipo Empleado
		
		Empleados Empleado1 = new DirectorEmpleado();
		
		
		//Uso de los objetos creados
		
		System.out.print(Empleado1.getTareas());
		*/
		
		
		ClassPathXmlApplicationContext contexto=new ClassPathXmlApplicationContext("applicationContext.xml");
		//ESTO CARGA EL ARCHIVO XML
		
		//EJ CON CONSTRUCTOR
		DirectorEmpleado Juan = contexto.getBean("miEmpleado",DirectorEmpleado.class);
		//Requiere el nombre del Bean creado anteriormente en applicationContext (DirectorEmpleado) y la clase creada anteriormente
		
		System.out.println(Juan.getTareas());
		
		System.out.println(Juan.getInforme());
		
		System.out.println("Email: " + Juan.getEmail());

		System.out.println(Juan.getNombreEmpresa());
	
		//EJ CON SETTERS
		/*Empleados Maria = contexto.getBean("miSecretarioEmpleado",Empleados.class);
		
		System.out.println(Maria.getTareas());
		
		System.out.println(Maria.getInforme());*/
		
		
		
		//INYECCION DE CAMPO
		/*SecretarioEmpleado Maria = contexto.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
		
		System.out.println(Maria.getTareas());
		
		System.out.println(Maria.getInforme());
		
		System.out.println("Email: " + Maria.getEmail());

		System.out.println(Maria.getNombreEmpresa());*/
		
		contexto.close();
		//Se debe CERRAR el contexto
	}

}
