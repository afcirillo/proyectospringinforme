package IoC;

public class DirectorEmpleado implements Empleados {
	
	//Creacion de campo tipo CreacionInformes (Interfaz)
	
	private CreacionInformes informeNuevo;
	
	private String email;
	
	private String nombreEmpresa;
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Gestionar la plantilla de la empresa";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return "Informe creado por el Director: " + informeNuevo.getInforme();
	}
	
	
	//Creacion de Construcotr que inyecta la dependecia
		public DirectorEmpleado(CreacionInformes informeNuevo) {
			
			this.informeNuevo=informeNuevo;
		}
		
	
	
	//Metodo init. Ejectuar tareas antes de que el bean este disponible
	public void metodoInicial() {
		System.out.println("Dentro del metodo init aqui irian las tareas a ejecutar"+
							"antes de que el bean este listo ");
	}
	
	//Metodo destroy. Ejecutar tareas despues de que el bean haya sdo ejecutado
	public void metodoFinal() {
		System.out.println("Dentro del metodo destroy aqui irian las tareas a ejecutar"+
							"despues de utilizar el bean");
	}

		
}
